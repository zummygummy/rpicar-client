import threading
from threading import Thread, Lock
import io
import sys
import time
import picamera
from time import sleep
import pigpio
import cv2
import tensorflow as tf
import model
from numpy import array
from picamera.array import PiRGBArray


PWM_PIN = 18
MOTOR_DATA_ONE = 17
MOTOR_DATA_TWO = 27
SERVO_DATA_ONE = 19
SERVO_DATA_TWO = 26
AUTO_DATA = 7

host = "192.168.0.20"
port = 8000

pi = pigpio.pi()

pi.write(SERVO_DATA_ONE, 1)  # set local Pi's gpio 4 low
pi.write(SERVO_DATA_TWO, 0)  # set tom's gpio 4 to high
pi.write(AUTO_DATA, 1)

train_vars = tf.trainable_variables()
sess = tf.InteractiveSession()
saver = tf.train.Saver()
saver.restore(sess, "model_100_16.ckpt")

mutex = Lock()

def pwm(turn_angle = int, data_one = int, data_two = int):
    pi.set_servo_pulsewidth(18 ,turn_angle)
    pi.write(MOTOR_DATA_ONE, data_one)  # set local Pi's gpio 4 low
    pi.write(MOTOR_DATA_TWO, data_two)  # set  gpio 4 to high


def steer(degrees):
    try:
        data_one = 0
        data_two = 1
        turn_angle = degrees*10
        pulses = 1600 - turn_angle
        print(degrees, pulses)
        t = Thread(target=pwm, args=(pulses, data_one, data_two))
        t.start()
    except:
        t = Thread(target=pwm, args=(0000, 0, 0))
        t.start()


def VideoStream():
    global command_client
    sleep(0.5)
    try:
        camera = picamera.PiCamera()
        camera.color_effects = (128, 128)
        camera.resolution = (320, 240)
        camera.vflip = True
        camera.hflip = True
        time.sleep(2)
        start = time.time()
        rawCapture = PiRGBArray(camera)
        for frame in camera.capture_continuous(rawCapture, 'bgr', use_video_port=True):
            image_array = frame.array
            height, width, color = image_array.shape
            roi = image_array[int(height / 4):height, :]
            image = cv2.resize(roi, (200,66)) / 255
            degrees = model.y.eval(session=sess, feed_dict={model.x: [image], model.keep_prob: 1.0})[0][
                          0] * 180.0 / 3.14159265359
            steer(degrees)
            rawCapture.truncate(0)
    finally:
        recvCommand = "000000"
        print("Error! Falha na camera!")

Thread(target=VideoStream).start()


