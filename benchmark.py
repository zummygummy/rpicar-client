import threading
from threading import Thread, Lock
import io
import socket
import struct
from time import time, strftime, localtime, sleep
from datetime import timedelta
import picamera
from time import sleep
import pigpio
import os

PWM_PIN = 18
MOTOR_DATA_ONE = 17
MOTOR_DATA_TWO = 27
SERVO_DATA_ONE = 19
SERVO_DATA_TWO = 26
AUTO_DATA = 7

# Set server IP address & Port
host = "192.168.0.20"
port = 8000

pi = pigpio.pi()

pi.write(SERVO_DATA_ONE, 1)  # set local Pi's gpio 4 low
pi.write(SERVO_DATA_TWO, 0)  # set tom's gpio 4 to high
pi.write(AUTO_DATA, 1)

global time_1
global time_2

# Thread to handle steering commands
mutex = Lock()

# def throttle(data1 = bool, data2= bool):
#     mutex.acquire()
#     try:
#         GPIO.output(BACK_MOTOR_DATA_ONE, data1)
#         GPIO.output(BACK_MOTOR_DATA_TWO, data2)
#
#     finally:
#         mutex.release()



def pwm(turn_angle = int, data_one = int, data_two = int, data_auto = int):
    mutex.acquire()
    time_2 = time()
    print("Tempo decorrido: " + str(int(1000 * (time_2 - time_1))).zfill(4) + " \n ")

    with open("output.txt", "a") as text_file:
        print("{} ".format(str(int(1000 * (time_2 - time_1))).zfill(4)), file=text_file)

    mutex.release()

    pi.set_servo_pulsewidth(18 ,turn_angle)
    pi.write(MOTOR_DATA_ONE, data_one)  # set local Pi's gpio 4 low
    pi.write(MOTOR_DATA_TWO, data_two)  # set  gpio 4 to high
    pi.write(AUTO_DATA, data_auto)


def steer():
    global command_client
    global dir

    send_inst = True

    try:
        while send_inst:
            sleep(0.1)
            # Read command received from server
            recvCommand = command_client.recv(1024)
            # print(recvCommand.decode())
            dir = recvCommand.decode()
            # print(dir)
            # Quit if the received command is "q" or empty string
            if (recvCommand == "q" or recvCommand == ""):
                command_client.close
                print('Exit')
                send_inst = False
                break;
            else:
                dir = recvCommand.decode()
                # print(dir)
                # print(recvCommand.decode())

            data_one = 0
            data_two = 0

            angle = 100 * int(dir[1])+ 10 * int(dir[2]) + int(dir[3])
            # print(angle)

            # print(int(dir[4]))
            # print(int(dir[5]))

            if int(dir[4]) == 0 and int(dir[5]) > 0:
                data_one = 0
                data_two = 1

            if int(dir[4]) == 1 and int(dir[5]) > 0:
                data_one = 1
                data_two = 0

            if int(dir[6]) == 1:
                data_auto = 1
            if int(dir[6]) == 0:
                data_auto = 0

            turn_angle = angle * 50 / 255

            if int(dir[0]) == 0:
                turn_angle *= -1

            # print(turn_angle*6 + 1500)
            turn_angle = turn_angle*6.5 + 1600
            # print(turn_angle)
            t = Thread(target=pwm, args=(turn_angle, data_one, data_two, data_auto))
            t.start()


    except:
        # Stop and reset car if an error occurs
        recvCommand = "00000000"
        print("Error! Connection lost!")


# Thread to handle video transmission
def VideoStream():
    global command_client
    global time_1
    # Initialise everything
    print("Connecting to command server")
    command_client = socket.socket()  # Create a socket object
    command_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    command_client.connect((host, port))  # Bind to the port
    print("Should be connected to command server")
    print("")

    # Start thread to handle control requests
    Thread(target=steer).start()

    # Wait 0.5 seconds to allow video server to initialise
    sleep(0.5)

    # Set up video client
    print("Connecting to video server")
    video_socket = socket.socket()
    video_socket.connect((host, port + 1))

    # Make a file-like object out of the connection
    connection = video_socket.makefile('wb')
    print("Should be connected to video server")
    print("")
    try:
        camera = picamera.PiCamera()
        camera.color_effects = (128, 128)
        camera.resolution = (320, 240)
        # Set to true if camera is flipped vertically
        camera.vflip = True
        camera.hflip = True
        # Start a preview and let the camera warm up for 2 seconds
        # camera.start_preview()
        sleep(2)

        # Note the start time and construct a stream to hold image data
        # temporarily (we could write it directly to connection but in this
        # case we want to find out the size of each capture first to keep
        # our protocol simple)
        start = time()
        stream = io.BytesIO()
        for foo in camera.capture_continuous(stream, 'jpeg', use_video_port=True):
            # Write the length of the capture to the stream and flush to
            # ensure it actually gets sent
            connection.write(struct.pack('<L', stream.tell()))
            time_1 = time()
            connection.flush()
            # Rewind the stream and send the image data over the wire
            stream.seek(0)
            connection.write(stream.read())
            # If we've been capturing for more than 30 seconds, quit
            # if time.time() - start > 30:
            #    break
            # Reset the stream for the next capture
            stream.seek(0)
            stream.truncate()
        # Write a length of zero to the stream to signal we're done
        connection.write(struct.pack('<L', 0))
    finally:
        recvCommand = "000000"
        print("Error! Connection lost!")
        connection.close()
        video_socket.close()

    # Start threads

Thread(target=VideoStream).start()


