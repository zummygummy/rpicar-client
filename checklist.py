import picamera
from time import sleep
import pigpio

# Script for testing every command on Raspberry Pi associated to motors and camera

PWM_PIN = 18
MOTOR_DATA_ONE = 17
MOTOR_DATA_TWO = 27
SERVO_DATA_ONE = 19
SERVO_DATA_TWO = 26
AUTO_DATA = 7

pi = pigpio.pi()

pi.write(SERVO_DATA_ONE, 1)  # set local Pi's gpio 4 low
pi.write(SERVO_DATA_TWO, 0)  # set tom's gpio 4 to high
pi.write(AUTO_DATA, 1)

motor_status = False
servo_status = False
camera_status = False

def test_motors(seconds):
    global motor_status
    try:
        pi.write(MOTOR_DATA_ONE, 1)  # set local Pi's gpio 4 low
        pi.write(MOTOR_DATA_TWO, 0)  # set  gpio 4 to high
        sleep(seconds)
        pi.write(MOTOR_DATA_ONE, 0)  # set local Pi's gpio 4 low
        pi.write(MOTOR_DATA_TWO, 1)  # set  gpio 4 to high
        sleep(seconds)
        pi.write(MOTOR_DATA_ONE, 0)  # set local Pi's gpio 4 low
        pi.write(MOTOR_DATA_TWO, 0)  # set  gpio 4 to high
        motor_status = True
    except:
        motor_status = False

def test_servo():
    global servo_status
    pulse = 1600
    for i in range(2):
        try:
            while pulse <= 2000:
                pi.set_servo_pulsewidth(18 ,pulse)
                pulse += 40
                sleep(0.1)

            while pulse >= 1200:
                pi.set_servo_pulsewidth(18 ,pulse)
                pulse -= 40
                sleep(0.1)

            while True:
                pi.set_servo_pulsewidth(18 ,pulse)
                pulse += 40
                sleep(0.1)
                if pulse == 1600:
                    break
            servo_status = True

        except:
            servo_status = False


def test_camera():
    global camera_status
    try:
        camera = picamera.PiCamera()
        camera.start_preview()
        sleep(5)
        camera.stop_preview()
        camera_status = True
    except:
        camera_status = False

def checklist():
    errors = 0
    if motor_status == False:
        print("Erro no sinal do motor.")
        errors += 1
    if motor_status == True:
        print("Sinais dos motores ok.")
    if servo_status == False:
        print("Erro no sinal do servo motor.")
        errors += 1
    if servo_status == True:
        print("Sinais do servo motor ok.")
    if camera_status == False:
        print("Erro na camera.")
        errors += 1
    if camera_status == True:
        print("Camera ok.")

    if errors == 0:
        print("Flight checklist completo.")

def main():
    test_motors(2)
    sleep(1)
    test_servo()
    sleep(1)
    test_camera()
    sleep(2)
    checklist()
    exit()

main()


