import threading
from threading import Thread, Lock
import io
import socket
import struct
import time
import picamera
from time import sleep
import pigpio


PWM_PIN = 18
MOTOR_DATA_ONE = 17
MOTOR_DATA_TWO = 27
SERVO_DATA_ONE = 19
SERVO_DATA_TWO = 26
AUTO_DATA = 7

host = "192.168.0.20"
port = 8000

pi = pigpio.pi()

pi.write(SERVO_DATA_ONE, 1)  
pi.write(SERVO_DATA_TWO, 0)  
pi.write(AUTO_DATA, 1)

mutex = Lock()

def throttle(data1 = bool, data2= bool):
    mutex.acquire()
    try:
        GPIO.output(BACK_MOTOR_DATA_ONE, data1)
        GPIO.output(BACK_MOTOR_DATA_TWO, data2)

    finally:
        mutex.release()

def pwm(turn_angle = int, data_one = int, data_two = int, data_auto = int):
    pi.set_servo_pulsewidth(18 ,turn_angle)
    pi.write(MOTOR_DATA_ONE, data_one)
    pi.write(MOTOR_DATA_TWO, data_two) 
    pi.write(AUTO_DATA, data_auto)


def steer():
    global command_client
    global dir

    send_inst = True

    try:
        while send_inst:
            sleep(0.1)
            recvCommand = command_client.recv(1024)
            dir = recvCommand.decode()
            if (recvCommand == "q" or recvCommand == ""):
                command_client.close
                print('Exit')
                send_inst = False
                break;
            else:
                dir = recvCommand.decode()

            data_one = 0
            data_two = 0

            angle = 100 * int(dir[1])+ 10 * int(dir[2]) + int(dir[3])

            if int(dir[4]) == 0 and int(dir[5]) > 0:
                data_one = 0
                data_two = 1

            if int(dir[4]) == 1 and int(dir[5]) > 0:
                data_one = 1
                data_two = 0

            if int(dir[6]) == 1:
                data_auto = 1
            if int(dir[6]) == 0:
                data_auto = 0

            turn_angle = angle * 50 / 255

            if int(dir[0]) == 0:
                turn_angle *= -1

            print(turn_angle)
            turn_angle = turn_angle*6.5 + 1600
            print(turn_angle)
            t = Thread(target=pwm, args=(turn_angle, data_one, data_two, data_auto))
            t.start()


    except:
        print("Error")


def VideoStream():
    global command_client
    command_client = socket.socket()  
    command_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    command_client.connect((host, port)) 
    Thread(target=steer).start()
    sleep(0.5)
    video_socket = socket.socket()
    video_socket.connect((host, port + 1))
    connection = video_socket.makefile('wb')
    try:
        camera = picamera.PiCamera()
        camera.color_effects = (128, 128)
        camera.resolution = (320, 240)
        camera.vflip = True
        camera.hflip = True
        time.sleep(2)
        start = time.time()
        stream = io.BytesIO()
        for foo in camera.capture_continuous(stream, 'jpeg', use_video_port=True):
            connection.write(struct.pack('<L', stream.tell()))
            connection.flush()
            stream.seek(0)
            connection.write(stream.read())
            stream.seek(0)
            stream.truncate()
        connection.write(struct.pack('<L', 0))
    finally:
        print("Error!")
        connection.close()
        video_socket.close()

Thread(target=VideoStream).start()


